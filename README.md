# Simple Spring Boot REST API
* In this project I showcase my education from the course channel in the description.

## Technical Requirments
* Project: Maven
* Java version: 11 LTS
* Spring version: 2.3.4.RELEASE
* IDE: IntelliJ Community
* Db: Derby
* Tested in: Postman
## Folder Structure
![](screenshots/folderStruct.PNG)

## Initial steps of process
* I created a simple mock list of topics to test REST features.
* Then I replaced it with TopicRepository that will take DB work for me.

![](screenshots/ExplainComments.PNG)

## POSTMAN TESTING
### POST Topic request
![](screenshots/Post.PNG)

### POST Course request
![](screenshots/Post-java-streams-course.PNG)

### GET Topic request
![](screenshots/Get-javaTopic.PNG)

### PUT request
    * GET before PUT
![](screenshots/Get-java-streams-course.PNG)

    * PUT request on course
![](screenshots/Put-java-streams.PNG)

    * GET after PUT
![](screenshots/Get-after-Put-java-streams.PNG)

### DELETE request
![](screenshots/Delete-java-streams.PNG)
