package io.javabrains.springbootstarter.services;

import io.javabrains.springbootstarter.model.TopicModel;
import io.javabrains.springbootstarter.repositories.TopicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class TopicService {

    @Autowired
    private TopicRepository topicRepository;

    /* Mock list of topics removed.
    * List<TopicModel> topics = new ArrayList<>(Arrays.asList(
    * new Topic("spring","Spring Framework","Spring Framework Description"),
    * new Topic("java","Core Java", "Java Description"),
    * new Topic("javascript", "JavaScript", "JavaScript Description")
    * ))*/

    public List<TopicModel> getAllTopics() {
        //return topics;
        List<TopicModel> topics = new ArrayList<>();
        topicRepository.findAll().forEach(topics::add);
        return topics;
    }

    public TopicModel getTopic(String id) {
        //return topics.stream().filter(t -> t.getId().equals(id)).findFirst().get();
        return topicRepository.findById(id).get();
    }

    public void addTopic(TopicModel topicModel) {
        //topics.add(topicModel);
        topicRepository.save(topicModel);
    }

    public void updateTopic(TopicModel topicModel, String id) {
        /** -> List<TopicModel> topics
         * Previous impl was looping through the mock list of topics and than update
         * the entity based on matched id.
         */
        topicRepository.save(topicModel);
    }

    public void deleteTopic(String id) {
        //topics.removeIf(t -> t.getId().equals(id));
        topicRepository.deleteById(id);
    }
}
