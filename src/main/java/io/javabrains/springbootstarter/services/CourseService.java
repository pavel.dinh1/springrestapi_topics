package io.javabrains.springbootstarter.services;

import io.javabrains.springbootstarter.model.CourseModel;
import io.javabrains.springbootstarter.repositories.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseService {

    @Autowired
    private CourseRepository courseRepository;

    public List<CourseModel> getAllCourses(String topicId) {
        return courseRepository.findByTopicId(topicId);
    }

    public CourseModel getCourse(String id) {
        return courseRepository.findById(id).get();
    }

    public void addCourse(CourseModel courseModel) {
        courseRepository.save(courseModel);
    }

    public void updateCourse(CourseModel courseModel) {
        courseRepository.save(courseModel);
    }

    public void deleteCourse(String id) {
        courseRepository.deleteById(id);
    }
}
