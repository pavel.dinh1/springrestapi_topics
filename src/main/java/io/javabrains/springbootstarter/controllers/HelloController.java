package io.javabrains.springbootstarter.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	/* Simple testing for initial run of the Spring Application -> Just like 'Hello World !'. */
	@GetMapping("/hello")
	public String sayHi() {
		return "Hello World!";
	}
}