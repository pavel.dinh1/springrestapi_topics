package io.javabrains.springbootstarter.controllers;

import io.javabrains.springbootstarter.model.CourseModel;
import io.javabrains.springbootstarter.model.TopicModel;
import io.javabrains.springbootstarter.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CourseController {

    @Autowired
    private CourseService courseService;

    @GetMapping("/topics/{topicId}/courses")
    public List<CourseModel> getAllCourses(@PathVariable String topicId) {
        return courseService.getAllCourses(topicId);
    }

    @GetMapping("/topics/{topicId}/courses/{id}")
    public CourseModel getCourse(@PathVariable String id) {
        return courseService.getCourse(id);
    }

    @PostMapping("/topics/{topicId}/courses")
    public void addCourse(@RequestBody CourseModel course, @PathVariable String topicId) {
        course.setTopic(new TopicModel(topicId,"",""));
        courseService.addCourse(course);
    }

    @PutMapping("/topics/{topicId}/courses/{id}")
    public void updateCourse(@RequestBody CourseModel course, @PathVariable String topicId, @PathVariable String id) {
        course.setTopic(new TopicModel(topicId,"",""));
        courseService.updateCourse(course);
    }

    @DeleteMapping("/topics/{topicId}/courses/{id}")
    public void deleteCourse(@PathVariable String id) {
        courseService.deleteCourse(id);
    }
}