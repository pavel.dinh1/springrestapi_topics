package io.javabrains.springbootstarter.repositories;

import io.javabrains.springbootstarter.model.CourseModel;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CourseRepository extends CrudRepository<CourseModel,String> {
    public List<CourseModel> findByTopicId(String topicId);
}