package io.javabrains.springbootstarter.repositories;

import io.javabrains.springbootstarter.model.TopicModel;
import org.springframework.data.repository.CrudRepository;

public interface TopicRepository extends CrudRepository<TopicModel,String> {

}